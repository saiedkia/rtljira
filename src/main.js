
var styles = document.styleSheets;
for (var index = 0; index < styles.length; s++) {

    var docRules = styles[index];

    if (docRules.rules == undefined) { continue; }
    docRules.insertRule("textarea { direction : rtl}", docRules.rules.length);
    docRules.insertRule("#summary { direction : rtl}", docRules.rules.length);
}